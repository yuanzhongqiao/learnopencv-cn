<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">学习OpenCV</font></font></h1><a id="user-content-learnopencv" class="anchor-element" aria-label="永久链接：LearnOpenCV" href="#learnopencv"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://www.LearnOpenCV.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此存储库包含在我们的博客LearnOpenCV.com</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上共享的计算机视觉、深度学习和人工智能文章的代码</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">想成为人工智能专家吗？</font></font><a href="https://opencv.org/courses/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 的人工智能课程</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一个很好的起点。</font></font></p>
<a href="https://opencv.org/courses/" rel="nofollow">
<p align="center" dir="auto">
<img src="https://camo.githubusercontent.com/27085d0935b67cc689f62ca63a59400b241f85732b9ec2c99e80094ffaf29582/68747470733a2f2f6c6561726e6f70656e63762e636f6d2f77702d636f6e74656e742f75706c6f6164732f323032332f30312f41492d436f75727365732d42792d4f70656e43562d4769746875622e706e67" data-canonical-src="https://learnopencv.com/wp-content/uploads/2023/01/AI-Courses-By-OpenCV-Github.png" style="max-width: 100%;">
</p>
</a>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">博客文章列表</font></font></h2><a id="user-content-list-of-blog-posts" class="anchor-element" aria-label="永久链接：博客文章列表" href="#list-of-blog-posts"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">博客文章</font></font></th>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://learnopencv.com/yolov9-advancing-the-yolo-legacy/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv9：推进 YOLO 遗产</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv9-Advancing-the-YOLO-Legacy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fine-tuning-llms-using-peft/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PEFT 微调法学硕士</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-LLMs-using-PEFT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deciphering-llms/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任何深度：加速单眼深度感知</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Depth-Anything"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deciphering-llms/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解读法学硕士：从变形金刚到量化</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Deciphering-LLMs"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolo-loss-function-gfl-vfl-loss/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO 损失函数第 2 部分：GFL 和 VFL 损失</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLO-Loss-Functions-Part2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolov8-object-tracking-and-counting-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv8-使用 OpenCV 进行对象跟踪和计数</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv8-Object-Tracking-and-Counting-with-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/adas-stereo-vision/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ADAS 中的立体视觉：超越激光雷达的开创性深度感知</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ADAS-Stereo-Vision"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolo-loss-function-siou-focal-loss/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO 损失函数第 1 部分：SIoU 和 Focal 损失</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLO-Loss-Functions-Part1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/moving-object-detection-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行移动物体检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Moving-Object-Detection-with-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/3d-lidar-object-detection/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 ADAS 与关键点特征金字塔网络集成以实现 3D LiDAR 物体检测</font></font></a></td>
<td align="left"><a href="https://www.dropbox.com/scl/fi/3n1s68jtfkjmw2f5e5ctv/3D-LiDAR-Object-Detection.zip?rlkey=d8q6xvlxis4oxso4qki87omvc&amp;dl=1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/mastering-all-yolo-models" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">掌握从 YOLOv1 到 YOLO-NAS 的所有 YOLO 模型：论文解释（2024 年）</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/intro-to-gradcam/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GradCAM：增强可解释人工智能领域的神经网络可解释性</font></font></a></td>
<td align="left"><a href="https://www.dropbox.com/scl/fo/3p3sg5fnvhrvi9vp00i0w/h?rlkey=1x01uz5o7esex7p6c8r534iyn&amp;dl=1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/text-summarization-using-t5/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 T5 进行文本摘要：微调和构建 Gradio 应用程序</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Text-Summarization-using-T5-Fine-Tuning-and-Building-Gradio-App"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/3d-lidar-visualization/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Open3D 的 3D LiDAR 可视化：用于自动驾驶的 2D KITTI 深度框架案例研究</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/3D-LiDAR-Perception"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fine-tuning-t5/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调 T5：用于构建 Stack Overflow 标签生成器的 Text2Text 传输变压器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-T5-Text2Text-Transformer-for-Strack-Overflow-Tag-Generation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/segformer-fine-tuning-for-lane-detection" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SegFormer 🤗：微调以改进自动驾驶车辆的车道检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-SegFormer-For-Lane-Detection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fine-tuning-bert" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Hugging Face Transformer 微调 BERT</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-BERT-using-Hugging-Face-Transformers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolo-nas-pose" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO-NAS 姿势</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLO-NAS-Pose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/bert-bidirectional-encoder-representations-from-transformers/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BERT：来自 Transformer 的双向编码器表示</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/BERT-Bidirectional-Encoder-Representations-from-Transformers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/comparing-kerascv-yolov8-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">比较 2020 年全球小麦数据上的 KerasCV YOLOv8 模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Comparing-KerasCV-YOLOv8-Models-on-the-Global-Wheat-Data-2020"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/top-5-ai-papers-of-september-2023/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2023 年 9 月 Top 5 人工智能论文</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/advanced-driver-assistance-systems/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">赋予驾驶员权力：高级驾驶员辅助系统的兴起和作用</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/kerascv-deeplabv3-plus-semantic-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 KerasCV DeepLabv3+ 进行语义分割</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Semantic-Segmentation-using-KerasCV-with-DeepLabv3-Plus"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/object-detection-using-kerascv-yolov8/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 KerasCV YOLOv8 进行物体检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Object-Detection-using-KerasCV-YOLOv8"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/animal-pose-estimation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调 YOLOv8 姿势模型以进行动物姿势估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-tuning-YOLOv8-Pose-Models-for-Animal-Pose-Estimation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/top-5-ai-papers-of-august-2023/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2023 年 8 月 Top 5 人工智能论文</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fine-tuning-trocr-training-trocr-to-recognize-curved-text/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调 TrOCR - 训练 TrOCR 识别弯曲文本</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-TrOCR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/trocr-getting-started-with-transformer-based-ocr/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TrOCR - 基于 Transformer 的 OCR 入门</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TrOCR-Getting-Started-with-Transformer-Based-OCR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/facial-emotion-recognition/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面部情绪识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Facial-Emotion-Recognition"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/object-keypoint-similarity/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关键点检测中的对象关键点相似度</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Object-Keypoint-Similarity-in-Keypoint-Detection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/real-time-deep-sort-with-torchvision-detectors/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Torchvision 探测器进行实时深度排序</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Real_Time_Deep_SORT_using_Torchvision_Detectors"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/top-5-ai-papers-of-july-2023/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2023 年 7 月 Top 5 人工智能论文</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/medical-image-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学图像分割</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Medical-Image-Segmentation-Using-HuggingFace-&amp;-PyTorch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/weighted-boxes-fusion/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">物体检测中的加权框融合：与非极大值抑制的比较</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Weighted-Boxes-Fusion-in-Object-Detection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/medical-multi-label/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PyTorch 和 Lightning 进行医疗多标签分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Medical_Multi-label_Classification_with_PyTorch_&amp;_Lightning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/paddlepaddle/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PaddlePaddle 入门：探索对象检测、分割和关键点</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Introduction-to-PaddlePaddle"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/drone-programming-with-computer-vision/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用计算机视觉进行无人机编程初学者指南</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Drone-Programming-With-Computer-Vision-A-Beginners-Guide"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/building-pip-installable-package-pypi/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何构建 Pip 可安装包并上传到 PyPi</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/iou-loss-functions-object-detection/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IoU 损失函数可实现更快、更准确的物体检测</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/slicing-aided-hyper-inference/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">探索用于小物体检测的切片辅助超推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Exploring-Slicing-Aided-Hyper-Inference"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/face-recognition-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人脸识别模型、工具包和数据集的进步</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/train-yolo-nas-on-custom-dataset/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在自定义数据集上训练 YOLO NAS</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Train-YOLO-NAS-on-Custom-Dataset"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/train-yolov8-instance-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在自定义数据上训练 YOLOv8 实例分割</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Train-YOLOv8-Instance-Segmentation-on-Custom-Data"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolo-nas/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO-NAS：新的目标检测模型击败 YOLOv6 和 YOLOv8</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLO-NAS_Introduction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/segment-anything/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分割任何东西——图像分割的基础模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Segment-Anything-A-Foundation-Model-for-Image-Segmentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/video-to-slides-converter-using-background-subtraction/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 中的背景估计和帧差分功能构建视频到幻灯片转换器应用程序</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Build-a-Video-to-Slides-Converter-Application-using-the-Power-of-Background-Estimation-and-Frame-Differencing-in-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/a-closer-look-at-cvat-perfecting-your-annotations/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">仔细研究 CVAT：完善您的注释</font></font></a></td>
<td align="left"><a href="https://www.youtube.com/watch?v=yxX_0-zr-2U&amp;list=PLfYPZalDvZDLvFhjuflhrxk_lLplXUqqB" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YouTube</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/controlnet/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ControlNet - 实现卓越的图像生成结果</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ControlNet-Achieving-Superior-Image-Generation-Results"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/instructpix2pix/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InstructPix2Pix - 根据提示编辑图像</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/InstructPix2Pix-Edit-Images-With-Prompts"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-spring-gtc-2023-day-4/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA 春季 GTC 2023 第 4 天：以压轴精彩时刻高调结束！</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-spring-gtc-2023-day-3-digging-deeper-into-deep-learning-semiconductors-more/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA 春季 GTC 2023 第 3 天：深入探讨深度学习、半导体等！</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-spring-gtc-2023-day-2-jensens-keynote-the-iphone-moment-of-ai-is-here/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA 春季 GTC 2023 第 2 天：Jensen 的主题演讲和 AI 的 iPhone 时刻就在这里！</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-spring-gtc-2023-day-1-highlights-welcome-to-the-future/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA 春季 GTC 2023 第 1 天：欢迎来到未来！</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-gtc-spring-2023-curtain-raiser/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2023 年春季揭幕</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/stable-diffusion-generative-ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稳定扩散——生成人工智能的新范式</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Stable-Diffusion-A-New-Paradigm-in-Generative-AI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/opencv-face-recognition-api/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 人脸识别 – 人脸识别适用于 AI 生成的图像吗？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/denoising-diffusion-probabilistic-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">去噪扩散概率模型深入指南 - 从理论到实现</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Guide-to-training-DDPMs-from-Scratch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/rise-of-midjourney-ai-art/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从像素到绘画：中途人工智能艺术的兴起</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/mastering-dall-e-2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">掌握DALL·E 2：AI艺术生成的突破</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/ai-art-generation-tools/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用扩散模型的十大人工智能艺术生成工具</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/the-future-of-image-recognition-is-here-pytorch-vision-transformer/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像识别的未来就在这里：PyTorch Vision Transformer</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Vision_Transformer_PyTorch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/attention-mechanism-in-transformer-neural-networks/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解 Transformer 神经网络中的注意力机制</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Attention_Mechanism_Introduction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deploy-deep-learning-model-huggingface-spaces/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用拥抱面部空间和渐变来部署深度学习模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Deploying-a-Deep-Learning-Model-using-Hugging-Face-Spaces-and-Gradio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/train-yolov8-on-custom-dataset/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在自定义数据集上训练 YOLOv8 – 完整教程</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Train-YOLOv8-on-Custom-Dataset-A-Complete-Tutorial"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/image-generation-using-diffusion-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像生成的扩散模型简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Introduction-to-Diffusion-Models-for-Image-Generation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/building-automated-image-annotation-tool-pyopenannotate/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建自动图像注释工具：PyOpenAnnotate</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Building-An-Automated-Image-Annotation-Tool-PyOpenAnnotate/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/ultralytics-yolov8/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ultralytics YOLOv8：最先进的 YOLO 模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Ultralytics-YOLOv8-State-of-the-Art-YOLO-Models"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/getting-started-with-yolov5-instance-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv5 实例分割入门</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Getting-Started-with-YOLOv5-Instance-Segmentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deeplabv3-ultimate-guide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepLabv3 终极指南 - 使用 PyTorch 推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/The-ultimate-guide-to-deeplabv3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/ai-fitness-trainer-using-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 MediaPipe 的 AI 健身教练：深蹲分析</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/AI-Fitness-Trainer-Using-MediaPipe-Analyzing-Squats"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolor-paper-explanation-inference-an-in-depth-analysis/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YoloR - 论文解释与推理 - 深入分析</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YoloR-paper-explanation-analysis"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/automated-image-annotation-tool-using-opencv-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Python 的自动图像注释工具路线图</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Roadmap-To-an-Automated-Image-Annotation-Tool-Using-Python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/performance-comparison-of-yolo-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLO 目标检测模型的性能比较 - 深入研究</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fcos-anchor-free-object-detection-explained/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FCOS - 无锚物体检测解释</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FCOS-Inference-using-PyTorch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolov6-custom-dataset-training/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv6自定义数据集训练——水下垃圾检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv6-Custom-Dataset-Training-Underwater-Trash-Detection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/what-is-exif-data-in-images/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是图像中的 EXIF 数据？</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/What-is-EXIF-Data-in-Images"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/t-sne-t-distributed-stochastic-neighbor-embedding-explained/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">t-SNE：T 分布随机邻域嵌入解释</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/t-SNE-with-Tensorboard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/centernet-anchor-free-object-detection-explained/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CenterNet：对象作为点 - 无锚对象检测解释</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/centernet-with-tf-hub"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolov7-pose-vs-mediapipe-in-human-pose-estimation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人体姿势估计中的 YOLOv7 Pose 与 MediaPipe</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv7-Pose-vs-MediaPipe-in-Human-Pose-Estimation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolov6-object-detection/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv6 物体检测 – 论文解释与推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv6-Object-Detection-Paper-Explanation-and-Inference"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolox-object-detector-paper-explanation-and-custom-training/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOX 物体检测器论文解释和定制培训</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOX-Object-Detection-Paper-Explanation-and-Custom-Training"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/driver-drowsiness-detection-using-mediapipe-in-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Python 中使用 Mediapipe 检测驾驶员困倦</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Driver-Drowsiness-detection-using-Mediapipe-in-Python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/gtc-2022-big-bang-ai-announcements-everything-you-need-to-know/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GTC 2022 Big Bang AI 公告：您需要了解的一切</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-gtc-2022-the-most-important-ai-event-this-fall/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2022：今年秋季最重要的人工智能盛会</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/object-tracking-and-reidentification-with-fairmot/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 FairMOT 进行对象跟踪和重新识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Object-Tracking-and-Reidentification-with-FairMOT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/what-is-face-detection-the-ultimate-guide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是人脸检测？</font><font style="vertical-align: inherit;">– 2022 年终极指南</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Face-Detection-Ultimate-Guide"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/custom-document-segmentation-using-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档扫描仪：使用 PyTorch-DeepLabV3 进行自定义语义分割</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Document-Scanner-Custom-Semantic-Segmentation-using-PyTorch-DeepLabV3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/fine-tuning-yolov7-on-custom-dataset/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在自定义数据集上微调 YOLOv7</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Fine-Tuning-YOLOv7"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/Center-Stage-for-zoom-call-using-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 MediaPipe 的 Zoom Call 的中心舞台</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/CenterStage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/mean-average-precision-map-object-detection-model-evaluation-metric/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目标检测中的平均精度 (mAP)</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/yolov7-object-detection-paper-explanation-and-inference/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YOLOv7物体检测论文讲解与推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv7-Object-Detection-Paper-Explanation-and-Inference"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/pothole-detection-using-yolov4-and-darknet/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 YOLOv4 和 Darknet 进行坑洞检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Pothole-Detection-using-YOLOv4-and-Darknet"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/automatic-document-scanner-using-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 的自动文档扫描仪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Automatic-Document-Scanner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/demystifying-gpu-architectures-for-deep-learning-part-2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">揭秘深度学习的 GPU 架构：第 2 部分</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/gpu_arch_and_CUDA"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/demystifying-gpu-architectures-for-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">揭秘深度学习的 GPU 架构</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/gpu_arch_and_CUDA"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/intersection-over-unioniou-in-object-detection-and-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对象检测和分割中的交集 (IoU)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Intersection-over-Union-IoU-in-Object-Detection-and-Segmentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/understanding-multiple-object-tracking-using-deepsort/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 DeepSORT 了解多对象跟踪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Understanding-Multiple-Object-Tracking-using-DeepSORT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/optical-character-recognition-using-paddleocr/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PaddleOCR 进行光学字符识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Optical-Character-Recognition-using-PaddleOCR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/gesture-control-in-zoom-call-using-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Mediapipe 在 Zoom Call 中进行手势控制</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/zoom-gestures"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deep-dive-into-tensorflow-model-optimization-toolkit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深入研究 Tensorflow 模型优化</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/A-Deep-Dive-into-Tensorflow-Model-Optimization"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/depthai-pipeline-overview-creating-a-complex-pipeline/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DepthAI 管道概述：创建复杂管道</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OAK-DepthAi-Pipeline-Overview"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/tensorflow-lite-model-maker-create-models-for-on-device-machine-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow Lite Model Maker：为设备上机器学习创建模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Tensorflow-Lite-Model-Maker-Create-Models-for-On-Device-ML"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/tensorflow-lite-model-optimization-for-on-device-machine-learning" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow Lite：设备上机器学习的模型优化</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TensorFlow-Lite-Model-Optimization-for-On-Device-MachineLearning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/object-detection-with-depth-measurement-with-oak-d/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OAK-D 预训练模型进行物体检测和深度测量</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OAK-Object-Detection-with-Depth"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/custom-object-detection-training-using-yolov5/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 YOLOv5 进行自定义对象检测训练</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Custom-Object-Detection-Training-using-YOLOv5"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/object-detection-using-yolov5-and-opencv-dnn-in-c-and-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Yolov5 和 OpenCV DNN 进行物体检测 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Object-Detection-using-YOLOv5-and-OpenCV-DNN-in-CPP-and-Python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/create-snapchat-instagram-filters-using-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Mediapipe 创建 Snapchat/Instagram 过滤器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Create-AR-filters-using-Mediapipe"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/autosar-c-compliant-deep-learning-inference-with-tensorrt/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 TensorRT 进行符合 AUTOSAR C++ 的深度学习推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/industrial_cv_TensorRT_cpp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-gtc-2022-day-4-highlights-meet-the-new-jetson-orin/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2022 第 4 天亮点：认识新的 Jetson Orin</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-gtc-2022-day-3-highlights-deep-dive-into-hopper-architecture/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2022 第 3 天亮点：深入探讨 Hopper 架构</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/nvidia-gtc-2022-day-2-highlights/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2022 第 2 天亮点：Jensen 的主题演讲</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/gtc-day-1-highlights/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NVIDIA GTC 2022 第一天亮点：辉煌的开局</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/automatic-license-plate-recognition-using-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用Python自动车牌识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ALPR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/building-a-body-posture-analysis-system-using-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 MediaPipe 构建不良身体姿势检测和警报系统</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Posture-analysis-system-using-MediaPipe-Pose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-mediapipe/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MediaPipe简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Introduction-to-MediaPipe"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/disparity-estimation-using-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用深度学习进行视差估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Disparity-Estimation-Using-Deep-Learning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/how-to-build-chrome-dino-game-bot-using-opencv-feature-matching/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何使用 OpenCV 特征匹配构建 Chrome Dino 游戏机器人</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Chrome-Dino-Bot-using-OpenCV-feature-matching"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/top-10-sources-to-find-computer-vision-and-ai-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">寻找计算机视觉和人工智能模型的十大来源</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/multi-attribute-and-graph-based-object-detection/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多属性和基于图的目标检测</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/plastic-waste-detection-with-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">利用深度学习检测塑料废物</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Plastic-Waste-Detection-with-Deep-Learning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/ensemble-deep-learning-based-defect-classification-and-detection-in-sem-images/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SEM 图像中基于集成深度学习的缺陷分类和检测</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/building-industrial-embedded-deep-learning-inference-pipelines-with-tensorrt/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 TensorRT 构建工业嵌入式深度学习推理管道</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/industrial_cv_TensorRT_python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/transfer-learning-for-medical-images/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">医学图像的迁移学习</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/stereo-vision-and-depth-estimation-using-opencv-ai-kit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV AI 套件进行立体视觉和深度估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/oak-getting-started"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-opencv-ai-kit-and-depthai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV AI 套件和 DepthAI 简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/oak-getting-started"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/wechat-qr-code-scanner-in-opencv" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的微信二维码扫描器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/WeChat-QRCode-Scanner-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/ai-behind-the-diwali-2021-not-just-a-cadbury-ad/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2021 年排灯节背后的人工智能“不仅仅是吉百利广告”</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/model-selection-and-benchmarking-with-modelplace-ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Modelplace.AI 进行模型选择和基准测试</font></font></a></td>
<td align="left"><a href="https://modelplace.ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型动物园</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/real-time-style-transfer-in-a-zoom-meeting/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Zoom 会议中的实时风格转换</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/style-transfer-zoom"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-openvino-deep-learning-workbench/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenVino 深度学习工作台简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Introduction-to-OpenVino-Deep-Learning-Workbench"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/running-openvino-models-on-intel-integrated-gpu/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Intel 集成 GPU 上运行 OpenVino 模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Running-OpenVino-Models-on-Intel-Integrated-GPU"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/post-training-quantization-with-openvino-toolkit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenVino 工具包进行训练后量化</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Post-Training-Quantization-with-OpenVino-Toolkit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-intel-openvino-toolkit/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">英特尔 OpenVINO 工具套件简介</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/human-action-recognition-using-detectron2-and-lstm/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Detectron2 和 LSTM 进行人体动作识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Human-Action-Recognition-Using-Detectron2-And-Lstm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/paired-image-to-image-translation-pix2pix/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pix2Pix：PyTorch 和 TensorFlow 中的图像到图像转换</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Image-to-Image-Translation-with-GAN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/conditional-gan-cgan-in-pytorch-and-tensorflow/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 和 TensorFlow 中的条件 GAN (cGAN)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Conditional-GAN-PyTorch-TensorFlow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deep-convolutional-gan-in-pytorch-and-tensorflow/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 和 TensorFlow 中的深度卷积 GAN</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Deep-Convolutional-GAN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-generative-adversarial-networks/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成对抗网络 (GAN) 简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Intro-to-Generative-Adversarial-Network"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/human-pose-estimation-using-keypoint-rcnn-in-pytorch/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 PyTorch 中使用关键点 RCNN 进行人体姿势估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Keypoint-RCNN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/non-maximum-suppression-theory-and-implementation-in-pytorch" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非极大值抑制：PyTorch 中的理论与实现</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Non-Maximum-Suppression"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/mrnet-multitask-approach/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MRNet – 多任务方法</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/MRnet-MultiTask-Approach"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/generative-and-discriminative-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">生成模型和判别模型</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/playing-chromes-t-rex-game-with-facial-gestures/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过面部手势玩 Chrome 的 T-Rex 游戏</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Playing-Chrome-TRex-Game-with-Facial-Gestures"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/variational-autoencoder-in-tensorflow/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow 中的变分自动编码器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Variational-Autoencoder-TensorFlow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/autoencoder-in-tensorflow-2-beginners-guide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow 2 中的自动编码器：初学者指南</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Autoencoder-in-TensorFlow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/deep-learning-with-opencvs-dnn-module-a-definitive-guide/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV DNN 模块进行深度学习：权威指南</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Deep-Learning-with-OpenCV-DNN-Module"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/depth-perception-using-stereo-camera-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用立体相机的深度感知 (Python/C++)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Depth-Perception-Using-Stereo-Camera"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/contour-detection-using-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行轮廓检测 (Python/C++)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Contour-Detection-using-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/super-resolution-in-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的超分辨率</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/Super-Resolution-in-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/improving-illumination-in-night-time-images/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">改善夜间图像的照明</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Improving-Illumination-in-Night-Time-Images"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/introduction-to-video-classification-and-human-activity-recognition/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频分类和人类活动识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/video-classification-and-human-activity-recognition"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/how-to-use-opencv-dnn-module-with-nvidia-gpu-on-windows" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在 Windows 上将 OpenCV DNN 模块与 Nvidia GPU 结合使用</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OpenCV-dnn-gpu-support-Windows"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/opencv-dnn-with-gpu-support/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何将 OpenCV DNN 模块与 NVIDIA GPU 结合使用</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OpenCV-dnn-gpu-support-Linux"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/code-opencv-in-visual-studio/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Visual Studio 中编写 OpenCV 代码</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/install-opencv-on-windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 OpenCV – C++ / Python</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Install-OpenCV-Windows-exe"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/face-recognition-with-arcface/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 ArcFace 进行人脸识别</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Face-Recognition-with-ArcFace"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/background-subtraction-with-opencv-and-bgs-libraries/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 和 BGS 库进行背景扣除</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Background-Subtraction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/optical-flow-using-deep-learning-raft/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RAFT：使用深度学习的光流估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Optical-Flow-Estimation-using-Deep-Learning-RAFT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/making-a-low-cost-stereo-camera-using-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 制作低成本立体相机</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/stereo-camera"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/optical-flow-in-opencv" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的光流 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Optical-Flow-in-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/introduction-to-epipolar-geometry-and-stereo-vision/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对极几何和立体视觉简介</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/EpipolarGeometryAndStereoVision"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/classification-with-localization/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本地化分类：将任何 keras 分类器转换为检测器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Classification-with-localization-convert-any-keras-classifier-into-a-detector/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/photoshop-filters-in-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的 Photoshop 滤镜</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Photoshop-Filters-in-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/tetris-with-opencv-python" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV Python 的俄罗斯方块游戏</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Tetris"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-with-opencv-for-android/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Android 上使用 OpenCV 进行图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/DNN-OpenCV-Classification-Android"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-with-opencv-java" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV Java 进行图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/DNN-OpenCV-Classification-with-Java"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/pytorch-to-tensorflow-model-conversion/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 到 Tensorflow 模型转换</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-to-TensorFlow-Model-Conversion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/snake-game-with-opencv-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV Python 进行贪吃蛇游戏</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/SnakeGame"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/stanford-mrnet-challenge-classifying-knee-mris/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯坦福大学 MRNet 挑战赛：膝关节 MRI 分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/MRNet-Single-Model"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/experiment-logging-with-tensorboard-and-wandb" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 TensorBoard 和 wandb 进行实验记录</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Vision-Experiment-Logging"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/understanding-lens-distortion/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解镜头畸变</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/UnderstandingLensDistortion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-matting-with-state-of-the-art-method-f-b-alpha-matting/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用最先进的方法“F、B、Alpha Matting”进行图像抠图</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FBAMatting"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/bag-of-tricks-for-image-classification-lets-check-if-it-is-working-or-not/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像分类技巧包 - 让我们检查一下它是否有效</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Bag-Of-Tricks-For-Image-Classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/getting-started-opencv-cuda-module/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV CUDA 模块入门</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Getting-Started-OpenCV-CUDA-Module"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/training-a-custom-object-detector-with-dlib-making-gesture-controlled-applications/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 DLIB 训练自定义对象检测器并制作手势控制应用程序</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Training_a_custom_hand_detector_with_dlib"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/how-to-run-inference-using-tensorrt-c-api/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何使用 TensorRT C++ API 运行推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-ONNX-TensorRT-CPP"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/using-facial-landmarks-for-overlaying-faces-with-masks/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用面部标志在脸上覆盖医用口罩</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FaceMaskOverlay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/tensorboard-with-pytorch-lightning" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PyTorch Lightning 的 Tensorboard</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TensorBoard-With-Pytorch-Lightning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/otsu-thresholding-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Otsu 使用 OpenCV 进行阈值处理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/otsu-method"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/pytorch-to-coreml-model-conversion/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 到 CoreML 模型转换</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-to-CoreML-model-conversion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/playing-rock-paper-scissors-with-ai/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与AI玩石头剪刀布</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Playing-rock-paper-scissors-with-AI"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/cnn-receptive-field-computation-using-backprop-with-tensorflow/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Backprop 和 TensorFlow 进行 CNN 感受野计算</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TensorFlow-Receptive-Field-With-Backprop"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/cnn-fully-convolutional-image-classification-with-tensorflow" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 TensorFlow 进行 CNN 全卷积图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TensorFlow-Fully-Convolutional-Image-Classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/how-to-convert-a-model-from-pytorch-to-tensorrt-and-speed-up-inference/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何将模型从 PyTorch 转换为 TensorRT 并加快推理速度</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-ONNX-TensorRT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/efficient-image-loading/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高效的图像加载</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Efficient-image-loading"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/graph-convolutional-networks-model-relations-in-data/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图卷积网络：数据中的模型关系</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Graph-Convolutional-Networks-Model-Relations-In-Data"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/federated-learning-using-pytorch-and-pysyft/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PyTorch 和 PySyft 进行联合学习入门</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Federated-Learning-Intro"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/creating-a-virtual-pen-and-eraser-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">创建虚拟笔和橡皮擦</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Creating-a-Virtual-Pen-and-Eraser"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/getting-started-with-pytorch-lightning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 闪电网络入门</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Pytorch-Lightning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/multi-label-image-classification-with-pytorch-image-tagging/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 PyTorch 进行多标签图像分类：图像标记</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Multi-Label-Image-Classification-Image-Tagging"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/Funny-Mirrors-Using-OpenCV/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 的有趣镜子</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FunnyMirrors"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/t-sne-for-feature-visualization/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于 ResNet 特征可视化的 t-SNE</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TSNE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/multi-label-image-classification-with-pytorch/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Pytorch 进行多标签图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Multi-Label-Image-Classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/cnn-receptive-field-computation-using-backprop/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用反向传播的 CNN 感受野计算</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Receptive-Field-With-Backprop"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/cnn-receptive-field-computation-using-backprop-with-tensorflow/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Backprop 和 TensorFlow 进行 CNN 感受野计算</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TensorFlow-Receptive-Field-With-Backprop"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/augmented-reality-using-aruco-markers-in-opencv-(c++-python)/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 OpenCV（C++ 和 Python）中使用 AruCo 标记的增强现实</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/AugmentedRealityWithArucoMarkers"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/fully-convolutional-image-classification-on-arbitrary-sized-image/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任意尺寸图像的全卷积图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Fully-Convolutional-Image-Classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/camera-calibration-using-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行相机校准</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/CameraCalibration"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/geometry-of-image-formation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像形成的几何形状</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/ensuring-training-reproducibility-in-pytorch" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保 Pytorch 中训练的可重复性</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/gaze-tracking/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视线追踪</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/simple-background-estimation-in-videos-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行视频中的简单背景估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/VideoBackgroundEstimation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/applications-of-foreground-background-separation-with-semantic-segmentation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前景背景分离与语义分割的应用</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/app-seperation-semseg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/efficientnet-theory-code" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EfficientNet：理论+代码</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/EfficientNet"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/mask-r-cnn-instance-segmentation-with-pytorch/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 初学者：使用 PyTorch 进行 Mask R-CNN 实例分割</font></font></a></td>
<td align="left"><a href="/spmallick/learnopencv/blob/master/PyTorch-Mask-RCNN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/faster-r-cnn-object-detection-with-pytorch" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 初学者：使用 PyTorch 进行更快的 R-CNN 目标检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-faster-RCNN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/pytorch-for-beginners-semantic-segmentation-using-torchvision/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 初学者：使用 torchvision 进行语义分割</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-Segmentation-torchvision"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-using-pre-trained-models-using-pytorch/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 初学者：图像分类预训练模型的比较</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Image-classification-pre-trained-models/Image_Classification_using_pre_trained_models.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/pytorch-for-beginners-basics/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PyTorch 初学者：基础知识</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/PyTorch-for-Beginners/PyTorch_for_Beginners.ipynb"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/pytorch-model-inference-using-onnx-and-caffe2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 ONNX 和 Caffe2 进行 PyTorch 模型推理</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Inference-for-PyTorch-Models/ONNX-Caffe2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-using-transfer-learning-in-pytorch/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 PyTorch 中使用迁移学习进行图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Image-Classification-in-PyTorch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/hangman-creating-games-in-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hangman：在 OpenCV 中创建游戏</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Hangman"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-inpainting-with-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行图像修复 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Image-Inpainting"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/hough-transform-with-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行霍夫变换 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Hough-Transform"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/xeus-cling-run-c-code-in-jupyter-notebook/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xeus-Cling：在 Jupyter Notebook 中运行 C++ 代码</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/XeusCling"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/age-gender-classification-using-opencv-deep-learning-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 深度学习进行性别和年龄分类 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/AgeGender"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/invisibility-cloak-using-color-detection-and-segmentation-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行颜色检测和分割的隐形斗篷</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/InvisibilityCloak"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/fast-image-downloader-for-open-images-v4/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Open Images V4 快速图像下载器 (Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/downloadOpenImages"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-based-text-detection-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 基于深度学习的文本检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/TextDetectionEAST"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/video-stabilization-using-point-feature-matching-in-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 OpenCV 中使用点特征匹配实现视频稳定</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/VideoStabilization"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/training-yolov3-deep-learning-based-custom-object-detector/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练 YOLOv3：基于深度学习的自定义对象检测器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/YOLOv3-Training-Snowman-Detector"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/using-openvino-with-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 OpenVINO 与 OpenCV 结合使用</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OpenVINO-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/duplicate-search-on-quora-dataset/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quora 数据集上的重复搜索</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Quora-Dataset-Duplicate-Search"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/shape-matching-using-hu-moments-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Hu 矩进行形状匹配 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/HuMoments"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-centos-7/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 CentOS（C++ 和 Python）上安装 OpenCV 4</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-on-centos.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-centos-7/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 CentOS（C++ 和 Python）上安装 OpenCV 3.4.4</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-on-centos.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-red-hat/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Red Hat 上安装 OpenCV 3.4.4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-on-red-hat.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-red-hat/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Red Hat 上安装 OpenCV 4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-4-on-red-hat.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-macos/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 macOS 上安装 OpenCV 4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/InstallScripts/installOpenCV-4-macos.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-raspberry-pi/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在树莓派上安装 OpenCV 3.4.4</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-raspberry-pi.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-macos/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 macOS 上安装 OpenCV 3.4.4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-macos.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/opencv-qr-code-scanner-c-and-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 二维码扫描器（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/QRCode-OpenCV"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 OpenCV 3.4.4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/InstallScripts/Windows-3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-ubuntu-16-04/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 16.04 上安装 OpenCV 3.4.4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-on-Ubuntu-16-04.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-4-4-on-ubuntu-18-04/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 18.04 上安装 OpenCV 3.4.4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-3-on-Ubuntu-18-04.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/universal-sentence-encoder" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通用句子编码器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/Universal-Sentence-Encoder"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-raspberry-pi/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在树莓派上安装 OpenCV 4</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-4-raspberry-pi.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 OpenCV 4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/InstallScripts/Windows-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人脸检测 – Dlib、OpenCV 和深度学习 (C++ / Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FaceDetectionComparison"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/hand-keypoint-detection-using-deep-learning-and-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用深度学习和 OpenCV 进行手部关键点检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/HandPose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-based-object-detection-and-instance-segmentation-using-mask-r-cnn-in-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 OpenCV 中使用 Mask R-CNN 进行基于深度学习的对象检测和实例分割（Python / C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Mask-RCNN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-ubuntu-18-04/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 18.04 上安装 OpenCV 4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-4-on-Ubuntu-18-04.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-4-on-ubuntu-16-04/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 16.04 上安装 OpenCV 4（C++ 和 Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/blob/master/InstallScripts/installOpenCV-4-on-Ubuntu-16-04.sh"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/multi-person-pose-estimation-in-opencv-using-openpose/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenPose 在 OpenCV 中进行多人姿势估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OpenPose-Multi-Person"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/heatmap-for-logo-detection-using-opencv-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (Python) 进行徽标检测的热图</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/heatmap"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-based-object-detection-using-yolov3-with-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 YOLOv3 和 OpenCV 进行基于深度学习的目标检测（Python / C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ObjectDetection-YOLO"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/convex-hull-using-opencv-in-python-and-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Python 和 C++ 中使用 OpenCV 的凸包</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ConvexHull"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/multitracker-multiple-object-tracking-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MultiTracker：使用 OpenCV (C++/Python) 进行多对象跟踪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/MultiObjectTracker"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/convolutional-neural-network-based-image-colorization-using-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 基于卷积神经网络的图像着色</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Colorization"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/svm-using-scikit-learn-in-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 scikit-learn 的 SVM</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/SVM-using-Python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/goturn-deep-learning-based-object-tracking/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GOTURN：基于深度学习的对象跟踪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/GOTURN"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/find-center-of-blob-centroid-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 查找 Blob 的中心（质心）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/CenterofBlob"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/support-vector-machines-svm/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持向量机 (SVM)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/SVM-using-Python"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/batch-normalization-in-deep-networks/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度网络中的批量归一化</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/BatchNormalization"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-character-classification-using-synthetic-dataset/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用合成数据集进行基于深度学习的字符分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/CharClassification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-quality-assessment-brisque/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像质量评估：BRISQUE</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ImageMetrics"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/understanding-alexnet/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解 AlexNet</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-based-text-recognition-ocr-using-tesseract-and-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Tesseract 和 OpenCV 基于深度学习的文本识别 (OCR)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OCR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/deep-learning-based-human-pose-estimation-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行基于深度学习的人体姿势估计（C++/Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/OpenPose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/number-of-parameters-and-tensor-sizes-in-convolutional-neural-network/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">卷积神经网络 (CNN) 中的参数数量和张量大小</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/how-to-convert-your-opencv-c-code-into-a-python-module/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何将 OpenCV C++ 代码转换为 Python 模块</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/pymodule"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/cv4faces-best-project-award-2018/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CV4Faces：2018 年最佳项目奖</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/facemark-facial-landmark-detection-using-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Facemark：使用 OpenCV 进行面部标志检测</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FacialLandmarkDetection"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-alignment-feature-based-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 进行图像对齐（基于特征）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ImageAlignment-FeatureBased"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/barcode-and-qr-code-scanner-using-zbar-and-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 ZBar 和 OpenCV 的条形码和 QR 码扫描仪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/barcode-QRcodeScanner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/keras-tutorial-fine-tuning-using-pre-trained-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Keras 教程：使用预训练模型进行微调</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Keras-Fine-Tuning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/opencv-transparent-api/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 透明 API</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/face-reconstruction-using-eigenfaces-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 EigenFaces 进行人脸重建 (C++/Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ReconstructFaceUsingEigenFaces"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/eigenface-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 的特征脸</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/EigenFace"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/principal-component-analysis/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主成分分析</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/keras-tutorial-transfer-learning-using-pre-trained-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Keras 教程：使用预训练模型进行迁移学习</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Keras-Transfer-Learning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/keras-tutorial-using-pre-trained-imagenet-models/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Keras 教程：使用预先训练的 Imagenet 模型</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Keras-ImageNet-Models"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/technical-aspects-of-a-digital-slr/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数码单反相机的技术方面</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/using-harry-potter-interactive-wand-with-opencv-to-create-magic/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用哈利波特互动魔杖和 OpenCV 来创造魔法</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/install-opencv-3-and-dlib-on-windows-python-only/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 OpenCV 3 和 Dlib（仅限 Python）</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Keras 中使用卷积神经网络进行图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/KerasCNN-CIFAR"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/understanding-autoencoders-using-tensorflow-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Tensorflow (Python) 了解自动编码器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/DenoisingAutoencoder"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/best-project-award-computer-vision-for-faces/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最佳项目奖：人脸计算机视觉</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/understanding-activation-functions-in-deep-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解深度学习中的激活函数</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/image-classification-using-feedforward-neural-network-in-keras/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Keras 中使用前馈神经网络进行图像分类</font></font></a></td>
<td align="left"><a href="https://github.com/kromydas/learnopencv/tree/master/Keras-MLP-MNIST-Classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/exposure-fusion-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 进行曝光融合</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ExposureFusion"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="https://www.learnopencv.com/understanding-feedforward-neural-networks/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解前馈神经网络</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 进行高动态范围 (HDR) 成像</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/hdr"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/deep-learning-using-keras-the-basics" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Keras 进行深度学习 – 基础知识</font></font></a></td>
<td align="left"><a href="https://github.com/kromydas/learnopencv/tree/master/Keras-Linear-Regression"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/selective-search-for-object-detection-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于对象检测的选择性搜索 (C++ / Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/SelectiveSearch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/installing-deep-learning-frameworks-on-ubuntu-with-cuda-support/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 上安装支持 CUDA 的深度学习框架</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/parallel-pixel-access-in-opencv-using-foreach/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中使用 forEach 的并行像素访问</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/forEach"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/cvui-gui-lib-built-on-top-of-opencv-drawing-primitives/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cvui：基于 OpenCV 绘图基元构建的 GUI 库</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/UI-cvui"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-dlib-on-windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 Dlib</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-dlib-on-ubuntu/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 上安装 Dlib</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-opencv3-on-ubuntu/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Ubuntu 上安装 OpenCV3</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 读取、写入和显示视频</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/VideoReadWriteDisplay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-dlib-on-macos/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 MacOS 上安装 Dlib</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-opencv3-on-macos/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 MacOS 上安装 OpenCV 3</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-opencv3-on-windows/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Windows 上安装 OpenCV 3</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/get-opencv-build-information-getbuildinformation/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取 OpenCV 构建信息 ( getBuildInformation )</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/color-spaces-in-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的色彩空间 (C++ / Python)</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ColorSpaces"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/neural-networks-a-30000-feet-view-for-beginners/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">神经网络：初学者的 30,000 英尺视图</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/alpha-blending-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 进行 Alpha 混合</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/AlphaBlending"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/user-stories-how-readers-of-this-blog-are-applying-their-knowledge-to-build-applications/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用户故事：该博客的读者如何应用他们的知识来构建应用程序</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/how-to-select-a-bounding-box-roi-in-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在 OpenCV (C++/Python) 中选择边界框 (ROI)？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/automatic-red-eye-remover-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++ / Python) 自动红眼去除器</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/RedEyeRemover"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/bias-variance-tradeoff-in-machine-learning/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机器学习中的偏差-方差权衡</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/embedded-computer-vision-which-device-should-you-choose/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">嵌入式计算机视觉：您应该选择哪种设备？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/object-tracking-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++/Python) 进行对象跟踪</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/tracking"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/handwritten-digits-classification-an-opencv-c-python-tutorial/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手写数字分类：OpenCV ( C++ / Python ) 教程</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/digits-classification"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/training-better-haar-lbp-cascade-eye-detector-opencv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 训练更好的基于 Haar 和 LBP 级联的眼部检测器</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/deep-learning-book-gift-recipients/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度学习书籍礼物接收者</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/minified-opencv-haar-and-lbp-cascades/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缩小的 OpenCV Haar 和 LBP 级联</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ninjaEyeDetector"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/deep-learning-book-gift/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度学习书籍礼物</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/histogram-of-oriented-gradients/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定向梯度直方图</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/image-recognition-and-object-detection-part1/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像识别和目标检测：第 1 部分</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/head-pose-estimation-using-opencv-and-dlib/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 和 Dlib 进行头部姿势估计</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/HeadPose"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/live-cv/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时简历：计算机视觉编码应用程序</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/approximate-focal-length-for-webcams-and-cell-phone-cameras/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络摄像头和手机摄像头的大致焦距</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/configuring-qt-for-opencv-on-osx/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 OSX 上为 OpenCV 配置 Qt</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/qt-test"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/rotation-matrix-to-euler-angles/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">旋转矩阵到欧拉角</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/RotationMatrixToEulerAngles"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/speeding-up-dlib-facial-landmark-detector/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加速 Dlib 的面部标志检测器</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/warp-one-triangle-to-another-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (C++ / Python) 将一个三角形变形为另一个三角形</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/WarpTriangle"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/average-face-opencv-c-python-tutorial/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平均脸：OpenCV ( C++ / Python ) 教程</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FaceAverage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/face-swap-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行面部交换（C++/Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FaceSwap"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/face-morph-using-opencv-cpp-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行脸部变形 — C++ / Python</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FaceMorph"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/deep-learning-example-using-nvidia-digits-3-on-ec2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 EC2 上使用 NVIDIA DIGITS 3 的深度学习示例</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/nvidia-digits-3-on-ec2/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EC2 上的 NVIDIA DIGITS 3</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/homography-examples-using-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 的单应性示例 ( Python / C ++ )</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Homography"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/filling-holes-in-an-image-using-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV (Python / C++) 填充图像中的孔</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Holes"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/how-to-find-frame-rate-or-frames-per-second-fps-in-opencv-python-cpp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在 OpenCV ( Python / C++ ) 中查找帧速率或每秒帧数 (fps)？</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FPS"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/delaunay-triangulation-and-voronoi-diagram-using-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 的 Delaunay 三角测量和 Voronoi 图（C++ / Python）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Delaunay"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/opencv-c-vs-python-vs-matlab-for-computer-vision/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于计算机视觉的 OpenCV（C++ 与 Python）与 MATLAB</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/facial-landmark-detection/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面部标志检测</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/why-does-opencv-use-bgr-color-format/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么OpenCV使用BGR颜色格式？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/computer-vision-for-predicting-facial-attractiveness/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于预测面部吸引力的计算机视觉</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/FacialAttractiveness"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/applycolormap-for-pseudocoloring-in-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">applyColorMap 用于 OpenCV 中的伪着色 ( C++ / Python )</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Colormap"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/image-alignment-ecc-in-opencv-c-python/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 中的图像对齐 (ECC) ( C++ / Python )</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/ImageAlignment"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/how-to-find-opencv-version-python-cpp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在Python和C++中找到OpenCV版本？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/baidu-banned-from-ilsvrc-2015/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">百度被禁止参加ILSVRC 2015</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/opencv-transparent-api/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 透明 API</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/how-computer-vision-solved-the-greatest-soccer-mystery-of-all-times/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">计算机视觉如何解开有史以来最大的足球之谜</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/embedded-vision-summit-2015/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015年嵌入式视觉峰会</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/read-an-image-in-opencv-python-cpp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 OpenCV 中读取图像（Python、C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/imread"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/non-photorealistic-rendering-using-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行非真实感渲染（Python、C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/NonPhotorealisticRendering"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/seamless-cloning-using-opencv-python-cpp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 无缝克隆（Python、C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/SeamlessCloning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/opencv-threshold-python-cpp/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenCV 阈值（Python、C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/Threshold"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/blob-detection-using-opencv-python-c/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenCV 进行斑点检测（Python、C++）</font></font></a></td>
<td align="left"><a href="https://github.com/spmallick/learnopencv/tree/master/BlobDetector"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码</font></font></a></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/turn-your-opencv-Code-into-a-web-api-in-under-10-minutes-part-1/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 10 分钟内将您的 OpenCV 代码转变为 Web API — 第 1 部分</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/how-to-compile-opencv-sample-Code/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何编译OpenCV示例代码？</font></font></a></td>
<td align="left"></td>
</tr>
<tr>
<td><a href="http://www.learnopencv.com/install-opencv-3-on-yosemite-osx-10-10-x/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Yosemite (OSX 10.10.x) 上安装 OpenCV 3</font></font></a></td>
<td align="left"></td>
</tr>
</tbody>
</table>
</article></div>
